require 'spec_helper'

RSpec.describe JenkinsfileRunner::CLI do
  let(:argv) { [] }

  subject(:subject) { described_class.new(argv) }

  it { is_expected.to respond_to(:run) }

  context 'with init command' do
    let(:argv) do
      [
        'init',
        '--jfr-version', '1.2.3',
        '--jenkins-home', '/var/jenkins_home',
        '--jenkins-war', '/usr/share/jenkins/jenkins.war',
        '--agent-type', 'docker',
        '--build-output', 'path/to/output'
      ]
    end

    it { expect(subject.command.class).to eq(JenkinsfileRunner::Commands::Init) }

    it 'parses configuration' do
      expect(subject.command.configuration)
        .to have_attributes(
          jfr_version: '1.2.3',
          jenkins_home: '/var/jenkins_home',
          jenkins_war: '/usr/share/jenkins/jenkins.war',
          agent_type: :docker,
          build_output: 'path/to/output')
    end
  end

  context 'with unknown command' do
    let(:argv) do
      [
        'bacon',
        '--cooked', 'crispy',
      ]
    end

    it { expect(subject.command.class).to eq(JenkinsfileRunner::Commands::Help) }
  end
end


