# frozen_string_literal: true

module JenkinsfileRunner
  module Commands
    class Help
      def initialize(argv)
      end

      def run
        puts "Command not found.\nAvailable commands: #{Command::COMMANDS.keys.join(', ')}"
      end
    end
  end
end
