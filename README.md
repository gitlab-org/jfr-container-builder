# Jenkinsfile Runner Container Builder

> **Disclaimer:** this tool is intended to solve some gaps when migrating from Jenkins to GitLab CI/CD. It is not meant to be used as a long-term solution.

This is a simple CLI tool that creates a container that includes your Jenkins installation,
making it possible to run a `Jenkinsfile` on GitLab CI/CD as one step of your plan to [migrate to GitLab](https://docs.gitlab.com/ee/ci/jenkins/index.html).
It is compatible with common `Jenkinsfile` configurations that build something, test it,
and produce an artifact. It is not compatible with complex pipelines, but can help
you move over a lot of your simple pipelines easily, letting you convert them to
`.gitlab-ci.yml` later.

It uses a simple process:

1. Create a Docker container which includes all the Jenkins configurations from the
   `$JENKINS_HOME` directory.
1. Include any installed Jenkins plug-ins.
1. Use a [specific version](#jenkinsfile-runner-compatibility) of [`jenkinsfile-runner`](https://github.com/jenkinsci/jenkinsfile-runner)
   which allows running a custom `Jenkinsfile`.
1. Upload the container to GitLab's container registry.

This tool is heavily inspired by ["Running Jenkins Files inside GitLab CI"](https://lackastack.gitlab.io/website/posts/gitlabci-jenkinsfile/).

## Requirements

System requirements:

- Ruby 2.4 or above
- Docker 19.x

Additionally:

- The project you want to build needs to have a Jenkinsfile.
- You need to have an account on a GitLab installation.
- You need to create a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with `api` scope.
- You need to [find out which version](#jenkinsfile-runner-compatibility) of
  `jenkinsfile-runner` is compatible with your Jenkins installation. Failing to use
  the right version will cause build or runtime errors.
- You need to [set up specific Runners](https://docs.gitlab.com/ee/ci/runners/) that have enough resources
  to execute an entire Jenkins pipeline. [Read more](#runner-sizing-and-configuration)

## Limitations

### Jenkinsfile Runner compatibility

This tool is based on [Jenkinsfile Runner](https://github.com/jenkinsci/jenkinsfile-runner),
which only works with specific Jenkins versions. If the Jenkins version is not supported,
it will likely throw errors at runtime.

We have successfully tested `jenkinsfile-runner` [1.0-beta-11](https://github.com/jenkinsci/jenkinsfile-runner/tree/1.0-beta-11)
with the following Jenkins versions so far:

- `2.176.4`
- `2.176.2`
- `2.164.3`
- `2.150.3`
- `2.138.4`

You can find which Jenkins version is supported by each `jenkinsfile-runner`
release at the [Jenkinsfile Runner project releases page](https://github.com/jenkinsci/jenkinsfile-runner/releases).

### Secrets

Secrets are automatically filtered out (see the output of the `build` step for what is not included).
You will need to either re-add them to the container, or fetch them at runtime as needed.

### Runner sizing and configuration

Using `jenkinsfile-runner` to evaluate a `Jenkinsfile` means that an entire Jenkins
pipeline may run on a single GitLab Runner instance. This may raise scalability issues,
especially regarding available resources.

For this reason using the Shared Runner fleet for this is discouraged since they are likely not large enough for running an entire pipeline.

Please follow our guide on [setting up a Runner](https://docs.gitlab.com/ee/ci/runners/)

### Using Docker agents

To use Docker agents, the `jenkinsfile-runner` needs to communicate with the docker engine to start up new containers and share build directories with them.
You need to edit your runner configuration to expose `/var/run/docker.sock` and `/tmp/jenkinsfileRunner.tmp/` as volumes.
`/tmp/jenkinsfileRunner.tmp/` can be any directory that is used for the `${JENKINS_BUILD_DIR}` variable. For example:

```toml
# /etc/gitlab-runner/config.toml
[runners.docker]
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock", "/tmp/jenkinsfileRunner.tmp/:/tmp/jenkinsfileRunner.tmp"]
```

### Artifacts

Artifact management (for example, `archiveArtifacts`) sections will need to be removed from your Jenkinsfiles and handled using our own [`artifacts` keyword](https://docs.gitlab.com/ee/ci/yaml/#artifacts). If they are left in they can cause exceptions and incorrect pipeline failures.

Be sure your pipeline does not clean up after itself after producing artifacts, or they will not be findable by GitLab.

### Incremental Builds

Since each invocation of the job uses a new container, incremental builds are not possible out of the box.

### Agent Labels

If your configuration relies on agent labels to find the right build host, you'll need to remove the requirement (that is, use `agent any`). Then modify your Dockerfile produced by the `build` step above to ensure your build environment is set up.

## More about migrating to GitLab

For more information please read our [Jenkins migration guide](https://docs.gitlab.com/ee/ci/jenkins/index.html).

## Setting up your project

To start, you should [import the source](https://docs.gitlab.com/ee/user/project/import/index.html) for the project
in question from your current SCM to GitLab, if this is not already done. This would be the repository
that contains your Jenkinsfile.

Be sure the container registry is enabled in this project. In most cases this will be automatic,
but if not you can check the [setup instructions here](https://docs.gitlab.com/ee/user/packages/container_registry/#enable-the-container-registry-for-your-project).

## Installation

To install the Jenkinsfile Runner Container Builder, run the following from the command line
from the Jenkins host you want to create a container from:

```shell
git clone https://gitlab.com/gitlab-org/jfr-container-builder.git
cd jfr-container-builder
```

## Usage

The `init` command below initializes a directory with all the requirements needed to build
a Docker container. It also generates a `Dockerfile` which can be customized before
running the `build` command:

```shell
bin/jfr-container-builder init \                 # Gather all dependencies and create a Dockerfile
  --jfr-version 1.0-beta-11 \                    # Version of jenkinsfile-runner compatible with Jenkins installation
  --jenkins-home /var/jenkins_home \             # JENKINS_HOME directory
  --jenkins-plugins /path/to/plugins \           # Optional path to plugins file or directory, defaults to `plugins` directory from jenkins-home
  --jenkins-war /usr/share/jenkins/jenkins.war \ # Path to jenkins.war file
  --agent-type [shell|docker] \                  # Jenkins agent currently in use. Supported agents are:
                                                 #   1. shell: agent running on the same Jenkins server
                                                 #   2. docker: when using Docker agents on the Jenkins server
  --build-output /path/to/tmp/work/dir           # Path to temporary working directory
```

Typically, at this point you will need to customize the Dockerfile in your `build-output` folder to add the steps necessary to install your build tools or otherwise set up your environment.
We recommend setting aside your Dockerfile after customizing it so that you can reapply your changes later if you need to re-run the `build` step.
If you want to add the files from `--build-output` to a repository, consider using [git-lfs](https://docs.gitlab.com/ee/administration/lfs/manage_large_binaries_with_git_lfs.html) to track the Java binaries.

The `build` command builds and pushes a Jenkins sandbox image to [GitLab container registry](https://docs.gitlab.com/ee/user/packages/container_registry/) of your project:

```shell
bin/jfr-container-builder build \                        # Build a Jenkins sandbox container and push it to GitLab container registry
  --registry registry.gitlab.com \                       # GitLab container registry
  --image my-username/test-project/jenkinsfile-runner \  # Image name inside the container registry:
                                                         # Full name will be: registry.gitlab.com/my-username/test-project/jenkinsfile-runner
  --build-output /path/to/tmp/work/dir                   # Path to temporary working directory
```

Note: You will be asked for a username and [Personal Access Token](#requirements) with permissions to access the container registry.
The Personal Access Token is only being used to log in and push the container to a GitLab's container registry, and will not be stored in the container itself.

After the job completes we log out of Docker, which may show the following warning on some OSes:

```plaintext
WARNING: could not erase credentials: error erasing credentials - err: exit status 1, out: `error erasing credentials - err: exit status 1, out: `The specified item could not be found in the keychain.`
```

At this point you can go to your project's Container Registry and verify that the container exists.

> Example: `https://gitlab.com/my-username/test-project/container_registry`

## Run the `Jenkinsfile` in a GitLab CI job

After the Jenkins sandbox container has been created, you can use the following example
in a new `.gitlab-ci.yml` file in the project you created:

```yaml
.jenkins-job:
  before_script:
    - export JENKINS_BUILD_DIR="/tmp/jenkinsfileRunner.tmp/${CI_JOB_ID}"
    - mkdir -p ${JENKINS_BUILD_DIR}
    - mkdir -p artifacts/
  after_script:
    - export JENKINS_BUILD_DIR="/tmp/jenkinsfileRunner.tmp/${CI_JOB_ID}"
    - cd "${JENKINS_BUILD_DIR}/workspace/job"
    # isolate any untracked files into artifacts/ directory
    - git ls-files --others -z | xargs -0 -n 1 -I {} cp --parents {} "${CI_PROJECT_DIR}/artifacts/"
    - rm -rf "${JENKINS_BUILD_DIR}"

run-jenkinsfile:
  extends: .jenkins-job
  # replace the image name with the one built with jfr-container-builder CLI
  image: registry.gitlab.com/my-username/test-project/jenkinsfile-runner:latest
  script:
    - jenkinsfile-runner -f ./Jenkinsfile --runHome ${JENKINS_BUILD_DIR}
  # The "artifacts" keyword defaults to storing all artifacts, but this can be changed.
  # See https://docs.gitlab.com/ee/ci/yaml/#artifacts for more details.
  artifacts:
    paths:
      - ${CI_PROJECT_DIR}/artifacts/
```

### Use artifacts on GitLab

The [`.jenkins-job` job template](#run-the-jenkinsfile-in-a-gitlab-ci-job) automatically
moves any artifacts into the `artifacts/` directory in the runner container. So, starting
with the following `Jenkinsfile` example that creates a JUnit artifact:

```groovy
pipeline {
  agent { docker { image 'ruby:2.6.1' } }
  stages {
    stage('test') {
      steps {
        sh 'bundle exec rspec --format RspecJunitFormatter --out test/reports/rspec.xml'
      }
      post {
        always {
          junit 'test/reports/rspec.xml'
        }
      }
    }
  }
}
```

After the job completes, the `test/reports/rspec.xml` would be available at
`artifacts/test/reports/rspec.xml`. Then, the following example will:

1. Upload an archive of any files created in `artifacts/`.
1. Generate and display a JUnit report in the Merge Request page from `artifacts/test/reports/rspec.xml`

```yaml
jenkins-build:
  extends: .jenkins-job
  image: registry.gitlab.com/my-username/test-project/jenkinsfile-runner:latest
  script:
    - jenkinsfile-runner -f ./Jenkinsfile --runHome ${JENKINS_BUILD_DIR}
  artifacts:
    paths:
      - artifacts/
    reports:
      junit: artifacts/test/reports/rspec.xml
```

### Run multiple Jenkinsfiles

You can run multiple jobs with different `Jenkinsfile` files in a single GitLab pipeline:

```yaml
image: registry.gitlab.com/my-username/test-project/jenkinsfile-runner:latest

run-jenkinsfile-1:
  extends: .jenkins-job
  script: jenkinsfile-runner -f ./Jenkinsfile-1 --runHome ${JENKINS_BUILD_DIR}

run-jenkinsfile-2:
  extends: .jenkins-job
  script: jenkinsfile-runner -f ./Jenkinsfile-2 --runHome ${JENKINS_BUILD_DIR}
```

These jobs will all run in parallel. If you want to run them in sequence,
define a job per [stage](https://docs.gitlab.com/ee/ci/yaml/#stage).

## Contributing

We are happy to accept community contributions. Please refer to [our guide](./CONTRIBUTING.md) for more details.
